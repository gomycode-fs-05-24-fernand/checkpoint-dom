const products = [ //Array of product object
    {
        id:1,
        name:"Macbook Air 2020",
        price:899,
        like:false,
    },
    {
        id:2,
        name:"Galaxy Watch 3",
        price:355,
        like:false,
    },
    {
        id:3,
        name:"Samsung Oddyssey OLED",
        price:2400,
        like:false,
    },
    {
        id:4,
        name:"Google Pixel 8a",
        price:1125,
        like:false,
    }
];

const cart = [];


function addToCart(product,quantity){ //Add element to the cart or update quantity

    if(cart.find(element => element.id == product)){//if product exist in the cart update quantity and if quantity is equal to 0 remove the product
        if(cart.find(element => element.id == product)["quantity"] == 0){
            removeFromCart(product);
        }else{
            cart.find(element => element.id == product)["quantity"] += quantity;
        }
        

    }else{//if product doesn't exist in the cart add it
        let obj = {};
        obj = products.find(element => element.id == product);
        obj["quantity"] = quantity;
        cart.push(obj);
    }
    showCart(cart);

}

function removeFromCart(product){//Remove element from the cart
    cart.splice(cart.findIndex(element => element.id == product),1);
    showCart(cart);
}


let button = document.querySelectorAll(".like-button");
button.forEach(element => {
    element.addEventListener("click",function(){
        //console.log(element.getAttribute('data-product-id'));
        likeProduct(element.getAttribute('data-product-id'));
    });
})

function likeProduct(productId) { //Like or unlike a product
    if(products.find(element => element.like == false && element.id == productId)){
        //console.log("already liked");
        products.find(element => element.id == productId).like = true;
        document.querySelector(`[data-product-id="${productId}"]`).setAttribute("fill","100");

        
    }else{
        products.find(element => element.id == productId).like = false;
        //console.log("already unliked");
        document.querySelector(`[data-product-id="${productId}"]`).setAttribute("fill","none");
        
    }

    console.log(products);
}



function showCart(cart){ //Update the cart or show the current cart on the screen
    cart_div = document.querySelector(".cart_div");
        cart_div.innerHTML = "";
    cart.forEach(element => {
        

        cart_item = document.createElement("div");
        cart_item.setAttribute("class","cart_item border p-4 border rounded-4 shadow");
        cart_div.appendChild(cart_item);

        cart_item_name = document.createElement("p");
        cart_item_name.setAttribute("class","cart_item_name fs-4 fw-bold");
        cart_item_name.innerHTML = element.name;
        cart_item.appendChild(cart_item_name);

        cart_item_price = document.createElement("p");
        cart_item_price.setAttribute("class","cart_item_price fs-5 fw-bold");
        cart_item_price.innerHTML = element.price * element.quantity + " $";
        cart_item.appendChild(cart_item_price);  

        cart_item_quantity = document.createElement("p");
        cart_item_quantity.setAttribute("class","cart_item_quantity  fs-6 fw-bold");
        cart_item_quantity.innerHTML = " x"+element.quantity;
        cart_item.appendChild(cart_item_quantity);  

        cart_item_add = document.createElement("button");
        cart_item_add.setAttribute("class","cart_item_add btn btn-secondary me-2");
        cart_item_add.setAttribute("onclick","addToCart("+element.id+",1)");
        cart_item_add.innerHTML = "+";
        cart_item.appendChild(cart_item_add);

        cart_item_remove = document.createElement("button");
        cart_item_remove.setAttribute("class","cart_item_remove btn btn-secondary me-4");
        cart_item_remove.setAttribute("onclick","addToCart("+element.id+",-1)");
        cart_item_remove.innerHTML = "-";
        cart_item.appendChild(cart_item_remove);

        cart_item_delete = document.createElement("button");
        cart_item_delete.setAttribute("class","cart_item_delete btn btn-danger");
        cart_item_delete.setAttribute("onclick","removeFromCart("+element.id+")");
        cart_item_delete.innerHTML = "DELETE";
        cart_item.appendChild(cart_item_delete);
    });

    total_price = document.querySelector(".total_price");
    total_price.innerHTML = cart.reduce((a,b) => a + b.price * b.quantity,0)+" $";
}